// soal 1
var nilai = 78
var indeks
if (nilai >= 85){
    indeks = 'A'
}
else if (nilai >= 75 && nilai < 85){
    indeks = 'B'
}
else if (nilai >= 65 && nilai < 75){
    indeks = 'C'
}
else if (nilai >= 55 && nilai < 65){
    indeks = 'D'
}
else if (nilai < 55){
    indeks = 'E'
}

console.log("indeks : " + indeks)

// soal 2
var tanggal = 29;
var bulan = 5;
var tahun = 2022;
switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        console.log("Bulan tidak valid")
}

console.log(tanggal + " "+ bulan + " "+ tahun)

// soal 3
var n = 3
for (let index = 1; index <= n; index++){
    console.log("#".repeat(index))
}
n = 7
for (let index = 1; index <= n; index++){
    console.log("#".repeat(index))
}

// soal 4
var m = 10
for (let index = 1; index <= m; index++) {
    let mod = index % 3
    switch (mod) {
        case 1:
            console.log(index + " - I love programming")
            break;
        case 2:
            console.log(index + " - I love Javascript")
            break;
        case 0:
            console.log(index + " - I love VueJS")
            console.log("=".repeat(index))
            break;
    }
}