// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort().forEach(function(hewan){
    console.log(hewan)
})

// soal 2
function introduce(param) {
    return "Nama saya " + param.name + ", umur saya " + param.age + " tahun, alamat saya di " + param.address + ", dan saya punya hobby yaitu " + param.hobby
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

// soal 3
function hitung_huruf_vokal(str) {
    var count = 0
    var vocal = ['a', 'i', 'u', 'e', 'o']
    for (let index = 0; index < str.length; index++) {
        if(vocal.includes(str.charAt(index).toLowerCase())){
            count++
        }
    }
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// soal 4
function hitung(val) {
    return 2*val-2
}
console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) ) 