// soal 1
const area = (length, width) => {
    return length * width
}

const perimeter = (length, width) => {
    return 2 * (length + width)
}

let luas = area(5, 6)
let keliling = perimeter(9, 10)
console.log(luas, keliling)

// soal 2
const newFunction = (firstName, lastName) => {
    return{
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName() 

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
var after = `lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)
console.log(after)