function next_date(tanggal, bulan, tahun) {
    var tgl_limit
    if (bulan == 2){
        if ((tahun % 4 == 0 && tahun % 100 != 0) || tahun % 400 == 0){
            tgl_limit = 29
        }
        else{
            tgl_limit = 28
        }
    }
    else if (bulan == 1 || 3 || 5 || 7 || 8 || 10 || 12){
        tgl_limit = 31
    }
    else{
        tgl_limit = 30
    }
    if (bulan == 12 && tanggal == tgl_limit){
        tahun++
        bulan = 1
        tanggal = 1
    }
    else if(tgl_limit == tanggal){
        bulan++
        tanggal = 1
    }
    else{
        tanggal++
    }
    switch (bulan) {
        case 1:
            bulan = "Januari"
            break;
        case 2:
            bulan = "Februari"
            break;
        case 3:
            bulan = "Maret"
            break;
        case 4:
            bulan = "April"
            break;
        case 5:
            bulan = "Mei"
            break;
        case 6:
            bulan = "Juni"
            break;
        case 7:
            bulan = "Juli"
            break;
        case 8:
            bulan = "Agustus"
            break;
        case 9:
            bulan = "September"
            break;
        case 10:
            bulan = "Oktober"
            break;
        case 11:
            bulan = "November"
            break;
        case 12:
            bulan = "Desember"
            break;
        default:
            console.log("Bulan tidak valid")
            return
    }   
    console.log(tanggal + " " + bulan + " " + tahun)
}
// contoh 1

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

// contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

// contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021
