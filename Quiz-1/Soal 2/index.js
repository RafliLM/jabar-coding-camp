function jumlah_kata(kalimat) {
    var kata = kalimat.split(" ")
    kata = kata.filter(function(x) {
        return x != ''
    })
    console.log(kata.length)
}
//Contoh

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "


jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4